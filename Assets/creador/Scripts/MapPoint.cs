﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MapPoint : MonoBehaviour {
/*
    public List<GameObject> points;
	private Connections connectionTemp;

    //for Load Scene
    GameObject Global;
    bool readJson = false;
    public GameObject JsonManager;

    void Awake()
    {
        Global = GameObject.Find("Global");
    }

	// Use this for initialization
	void Start () {
		connectionTemp = null;
	}
	
	// Update is called once per frame
	void Update () {
        //to make it short, it checks for clicks, points and then creates a Connection with both points stored in it, which
        //later will be used to draw the lines in "DrawLines"
        if (SceneManager.GetActiveScene().name.Equals("LoadJson")){
            if (!readJson)
            {
                createRelations();
                readJson = true;
            }
        }
        
	}

	public void newConnect() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
        if (SceneManager.GetActiveScene().name.Equals("Editor"))
        {
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (this.points.Count <= 2)
                {
                    points.Add(hit.transform.gameObject);
                    Debug.Log(hit.transform.gameObject.name);
                    if (this.points.Count == 2)
                    {
                        int fakeIndex = 0;
                        Connections connectionNew = new Connections(points[0], points[1]);
                        bool isConnection = false;
                        foreach (Connections connection in this.GetComponent<DrawLines>().connections)
                        {
                            if ((connectionNew.point1 == connection.point1
                                && connectionNew.point2 == connection.point2)
                                ||
                                (connectionNew.point1 == connection.point2
                                && connectionNew.point2 == connection.point1)
                            )
                            {
                                isConnection = true;
                                connectionTemp = connection;
                                break;
                            }
                            fakeIndex++;
                            Debug.Log(GetComponent<DrawLines>().connections.IndexOf(connection));
                        }
                        if (!isConnection)
                        {
                            GetComponent<DrawLines>().connections.Add(connectionNew);
                        }
                        else
                        {
                            Debug.Log(connectionTemp.point1 + " - " + connectionTemp.point2);
                            this.GetComponent<DrawLines>().connections.RemoveAt(fakeIndex);
                            Debug.Log(GetComponent<DrawLines>().connections.IndexOf(connectionTemp));
                            Debug.Log(connectionTemp.point2.name);
                            string name = connectionTemp.point2.name;
                        }
                        this.points.Clear();
                        isConnection = false;

                    }
                }

            }
        }
		
	}

    public void createRelations()
    {
        for (int i = 0; i < JsonManager.GetComponent<JsonManager>().GetLenghtOfWorldPoints(); i++)
        {
            for (int e = 0; e < JsonManager.GetComponent<JsonManager>().GetLenghtOfConxObj(i); e++)
            {
                GameObject mainPoint = GameObject.Find(JsonManager.GetComponent<JsonManager>().GetNameObj(i));
                GameObject search = GameObject.Find(JsonManager.GetComponent<JsonManager>().GetConxObj(i, e));
                points.Add(search);
                if (this.points.Count == 1)
                {
                    Connections connectionNew = new Connections(mainPoint, points[0]);
                    bool isConnection = false;
                    foreach (Connections connection in this.GetComponent<DrawLines>().connections)
                    {
                        if ((connectionNew.point1 == connection.point1
                            && connectionNew.point2 == connection.point2)
                            ||
                            (connectionNew.point1 == connection.point2
                            && connectionNew.point2 == connection.point1))
                        {
                            isConnection = true;
                        }
                    }
                    if (!isConnection)
                    {
                        GetComponent<DrawLines>().connections.Add(connectionNew);
                    }
                    else {
                        Debug.Log("Connection already exists!");
                    }
                    this.points.Clear();
                    isConnection = false;

                }
            }

        }
    }

	public bool areConnected(GameObject gobject)
    {
        GameObject worldController = GameObject.Find("World Controller");
        bool areConnected = false;
		GameObject sphere1 = worldController.GetComponent<WorldController>().currentSphere;
			Debug.Log (sphere1);
			Connections connectionNew2 = new Connections(sphere1, gobject);
			foreach (Connections connection in this.GetComponent<DrawLines>().connections)
			{
				if ((connectionNew2.point1 == connection.point1
					&& connectionNew2.point2 == connection.point2)
					||
					(connectionNew2.point1 == connection.point2
					&& connectionNew2.point2 == connection.point1)
				)
				{
					areConnected = true;
					Debug.Log ("Points are connected. Going to point.");
				} else {
					Debug.Log (connection.point1 + " " + connection.point2 + " - " + sphere1 + " " + gobject);
					Debug.Log ("Points aren't connected!!!!");
				}
			}
		return areConnected;
	}

    */
}
