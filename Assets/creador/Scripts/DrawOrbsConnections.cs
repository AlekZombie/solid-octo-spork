﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawOrbsConnections : MonoBehaviour
{

    public GameObject connectionsController;

    public Material lineMat; //quite important, I chose to set it on the inspector


    void Start()
    {

    }

    //connect the points
    void DrawConnectedLines()
    {
        if (connectionsController.GetComponent<OrbConnectionsController>().connectedOrbs.Count != 0)
        {
            foreach (OrbConnection connection in connectionsController.GetComponent<OrbConnectionsController>().connectedOrbs)
            {
                Vector3 mainPointPos = connection.orb1.transform.position;
                Vector3 pointPos = connection.orb2.transform.position;
                Vector3 midPoint = Vector3.Lerp(mainPointPos, pointPos, 0.5f);

                GL.Begin(GL.LINES);
                lineMat.SetPass(0);
                GL.Color(Color.clear);
                GL.Vertex3(mainPointPos.x, mainPointPos.y, mainPointPos.z);

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100)
                    && connectionsController.GetComponent<OrbConnectionsController>().orbs.Count == 1
                    && ((connection.orb1 == hit.transform.gameObject &&
                        connection.orb2 == connectionsController.GetComponent<OrbConnectionsController>().orbs[0]) ||
                        (connection.orb2 == hit.transform.gameObject &&
                        connection.orb1 == connectionsController.GetComponent<OrbConnectionsController>().orbs[0])))
                {
                    GL.Color(Color.red);
                }
                else
                    GL.Color(Color.black);

                GL.Vertex3(midPoint.x, midPoint.y, midPoint.z);
                GL.Vertex3(midPoint.x, midPoint.y, midPoint.z);
                GL.Color(Color.clear);
                GL.Vertex3(pointPos.x, pointPos.y, pointPos.z);
                GL.End();
            }
        }
    }

    void DrawConnectingLines()
    {
        if (connectionsController.GetComponent<OrbConnectionsController>().orbs.Count == 1)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (!Physics.Raycast(ray, out hit, 100) || (Physics.Raycast(ray, out hit, 100) &&
                string.IsNullOrEmpty(connectionsController.GetComponent<OrbConnectionsController>().connectionExists(
                connectionsController.GetComponent<OrbConnectionsController>().orbs[0], hit.transform.gameObject))))
            {
                Vector3 screenPoint = Input.mousePosition;
                screenPoint.z = 10;
                Vector3 worldPoint = Camera.main.ScreenToWorldPoint(screenPoint);

                Vector3 mainPointPos = connectionsController.GetComponent<OrbConnectionsController>().orbs[0].transform.position;
                Vector3 pointPos = worldPoint;
                Vector3 midPoint = Vector3.Lerp(mainPointPos, pointPos, 0.5f);

                GL.Begin(GL.LINES);
                lineMat.SetPass(0);
                GL.Color(Color.clear);
                GL.Vertex3(mainPointPos.x, mainPointPos.y, mainPointPos.z);
                GL.Color(Color.black);
                GL.Vertex3(midPoint.x, midPoint.y, midPoint.z);
                GL.Color(Color.black);
                GL.Vertex3(midPoint.x, midPoint.y, midPoint.z);
                GL.Color(Color.clear);
                GL.Vertex3(pointPos.x, pointPos.y, pointPos.z);
                GL.End();
            }

        }
    }

    // To show the lines in the game window whne it is running
    void OnPostRender()
    {
        DrawConnectedLines();
        DrawConnectingLines();
    }

    // To show the lines in the editor
    void OnDrawGizmos()
    {
        DrawConnectedLines();
        DrawConnectingLines();
    }
}