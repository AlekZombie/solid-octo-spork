﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class EditMenu : MonoBehaviour {
    /*
	public GameObject pointToEdit { get; set; }
	public GameObject changeDescriptionMenu;
	public GameObject changeVideoMenu;
	public GameObject changeImageMenu;
    public GameObject principalMenu;
    public Dropdown dropdownVideo;
	public Dropdown dropdownImage;

	void Awake(){
		SetCmbVideo();
		SetCmbImage ();
		dropdownVideo.onValueChanged.AddListener(delegate
		{
				dropdownVideoValuesChangedHandler(dropdownVideo);
		});
		dropdownImage.onValueChanged.AddListener(delegate
		{
				dropdownImageValuesChangedHandler(dropdownImage);
		});
	}

	public void ChangeDescription(string guess){
		pointToEdit.GetComponent<Point> ().description = guess;
	}

	public void showChangeDescriptionMenu(){
		changeDescriptionMenu.SetActive (true);
		principalMenu.transform.position=new Vector3(-3076, 0, 0);
	}

	public void hideChangeDescriptionMenu(){
		changeDescriptionMenu.SetActive (false);
	}

	public void showChangeVideoMenu(){
		changeVideoMenu.SetActive (true);
        principalMenu.transform.position = new Vector3(-3076, 0, 0);
    }

	public void hideChangeVideoMenu(){
		changeVideoMenu.SetActive (false);
	}

	public void showChangeImageMenu(){
		changeImageMenu.SetActive (true);
        principalMenu.transform.position = new Vector3(-3076, 0, 0);
    }

	public void hideChangeImageMenu(){
		changeImageMenu.SetActive (false);
	}

	void SetCmbVideo()
	{
	    DirectoryInfo directoryInfo = new DirectoryInfo(Application.dataPath + "/Videos");
	    FileInfo[] fileInfo = directoryInfo.GetFiles("*.mp4", SearchOption.AllDirectories);
		dropdownVideo.GetComponent<Dropdown>().options.Clear();
	    foreach (FileInfo file in fileInfo)
	    {
	        string nameFile = Path.GetFileNameWithoutExtension(file.FullName);
	        Dropdown.OptionData optionData = new Dropdown.OptionData(nameFile);
			dropdownVideo.GetComponent<Dropdown>().options.Add(optionData);
	    }
	}

	void SetCmbImage(){
		DirectoryInfo directoryInfo = new DirectoryInfo(Application.dataPath + "/Images");
		FileInfo[] fileInfo = directoryInfo.GetFiles("*.jpg", SearchOption.AllDirectories);
		dropdownImage.GetComponent<Dropdown>().options.Clear();
		foreach (FileInfo file in fileInfo)
		{
			string nameFile = Path.GetFileNameWithoutExtension(file.FullName);
			Dropdown.OptionData optionData = new Dropdown.OptionData(nameFile);
			dropdownImage.GetComponent<Dropdown>().options.Add(optionData);
		}
	}
    /*
	private void dropdownVideoValuesChangedHandler(Dropdown target)
	{
		GameObject label = dropdownVideo.transform.Find("Label").gameObject;
		pointToEdit.GetComponent<InterfacePoint> ().InsideTexture = null;
	    WWW www = new WWW("file://" + Application.dataPath + "/Videos/" + label.GetComponent<Text>().text + ".mp4");
		pointToEdit.GetComponent<UnityEngine.Video.VideoPlayer> ().url = "file://" + Application.dataPath + "/Videos/" + label.GetComponent<Text>().text + ".mp4";
	}

	private void dropdownImageValuesChangedHandler(Dropdown target){
		GameObject label = dropdownImage.transform.Find ("Label").gameObject;
		WWW www = new WWW("file://" + Application.dataPath + "/Images/" + label.GetComponent<Text>().text + ".jpg");
        pointToEdit.GetComponent<UnityEngine.Video.VideoPlayer>().url = "";
        pointToEdit.GetComponent<InterfacePoint>().InsideTexture = www.texture;
        pointToEdit.GetComponent<InterfacePoint>().insideTextureName = label.GetComponent<Text>().text;
	}
    */


}
