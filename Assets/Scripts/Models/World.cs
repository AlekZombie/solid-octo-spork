﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class World{
    /*
    public string WorldName { get; set; }
    public string WorldDesc { get; set; }
    public List<Point> WorldPoints { get; set; }

    public World()
    {
        this.WorldName = "";
        this.WorldDesc = "";
        this.WorldPoints = new List<Point>();
    }

    public void addSphere(Point p)
    {
        WorldPoints.Add(p);
    }

    public void CreateSphere()
    {
        GameObject Parent = GameObject.Find("World Controller");
        GameObject Global = GameObject.Find("Global");
        GameObject JsonManager = GameObject.Find("JsonManager");

        for (int i = 0; i < JsonManager.GetComponent<JsonManager>().GetLenghtOfWorldPoints(); i++)
        {
            GameObject point = Object.Instantiate(Resources.Load("Prefabs/SphereLoad")) as GameObject;

            //Obj's Properties  
            point.name = JsonManager.GetComponent<JsonManager>().GetNameObj(i);
            float x = JsonManager.GetComponent<JsonManager>().GetPosXObj(i);
            float z = JsonManager.GetComponent<JsonManager>().GetPosZObj(i);    
            point.transform.position = new Vector3(x, 0, z);
            point.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f);

            //Setting parent
            point.transform.SetParent(Parent.transform);

            // Modifying the Components
            //point.GetComponent<InterfacePoint>().description = JsonManager.GetComponent<JsonManager>().GetDescObj(i);
            //point.GetComponent<InterfacePoint>().currentDescription = string.Empty;
            
            if (!JsonManager.GetComponent<JsonManager>().GetImageObj(i).Equals(""))
            {
                WWW img = new WWW("file://" + Application.dataPath + "/Images/" + JsonManager.GetComponent<JsonManager>().GetImageObj(i) + ".jpg");
                point.GetComponent<InterfacePoint>().InsideTexture = img.texture;
            }

            if (!JsonManager.GetComponent<JsonManager>().GetVideoObj(i).Equals(""))
            {
                point.GetComponent<UnityEngine.Video.VideoPlayer>().url = JsonManager.GetComponent<JsonManager>().GetVideoObj(i);
            }
            //point.GetComponent<Point>().icon = img.texture;
            //point.GetComponent<Point>().currentIcon = null;
        }
    }
    public bool isExist(int id)
    {
        bool aux = false;
        foreach (Point point in WorldPoints)
        {
            aux = point.id == id ? true : false;
        }
        return aux;
    }

    public int Modify(GameObject point)
    {
        int index = 0;
        for (int i = 0; i < WorldPoints.Count; i++){
            if(WorldPoints[i].id == point.GetComponent<InterfacePoint>().id) {
                index = WorldPoints[i].id == point.GetComponent<InterfacePoint>().id ? i : -1;
            }
        }
        return index;
    }
    */
}
