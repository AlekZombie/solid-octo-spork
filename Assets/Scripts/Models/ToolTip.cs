﻿using UnityEngine;
using System.Collections;

public class ToolTip : MonoBehaviour
{

	public string toolTipText; // set this in World
	public Texture2D icon; // set this in World
	public string currentToolTipText { get; set; }
	public Texture2D currentIcon { get; set; }


	void OnMouseEnter()
	{
		if (!toolTipText.Equals(""))
		{
			currentToolTipText = toolTipText;
		}
		if (icon != null)
		{
			currentIcon = icon;
		}
	}

	void OnMouseExit()
	{
		currentToolTipText = "";
		currentIcon = null;
	}

	void OnGUI()
	{
		float x = Event.current.mousePosition.x;
		float y = Event.current.mousePosition.y;
		if (!string.IsNullOrEmpty(currentToolTipText))
		{
			GUI.Box(new Rect(x - 30, y + 20, 190, 130), currentToolTipText);
		}
		if (currentIcon != null)
		{
			GUI.Box(new Rect(x - 30, y + 20, 190, 130), currentIcon);
		}
		    if (currentIcon != null && !string.IsNullOrEmpty(currentToolTipText))
		{
			GUI.Box(new Rect(x + 160, y + 20, 190, 130), currentToolTipText);
			GUI.Box(new Rect(x - 30, y + 20, 190, 130), currentIcon);
		}
	}
}