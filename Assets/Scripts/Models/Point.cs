﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using LitJson;

public class Point {
    /* basic attributes */
    public int id { get; set;  }
    public string name { get; set; }
    public float posx { get; set; }
    public float posz { get; set; }
    public string description { get; set; }
	public string icon { get; set;}
    public string video { get; set; }
    public string image { get; set; }


	public Point(){
        id = 0;
        posx = 0;
        posz = 0;
		description = string.Empty;
		icon = string.Empty;
		video = string.Empty;
		image = string.Empty;
	}
		
}
