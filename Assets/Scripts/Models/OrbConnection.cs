﻿using UnityEngine;

public class OrbConnection : MonoBehaviour
{
    public GameObject orb1;
    public GameObject orb2;

    public OrbConnection(GameObject orb1, GameObject orb2)
    {
        this.orb1 = orb1;
        this.orb2 = orb2;
    }

}