﻿using UnityEngine;
using System.Collections;

public class GizmoPivot : MonoBehaviour
{

    public float gizmoSize = 0.75f;
    public Color gizmoColor = Color.yellow;
    public GameObject orb;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(orb.transform.position, gizmoSize);
    }
}
