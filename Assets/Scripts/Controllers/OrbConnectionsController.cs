﻿using System.Collections.Generic;
using UnityEngine;

public class OrbConnectionsController : MonoBehaviour {

    public List<GameObject> orbs;

    public List<OrbConnection> connectedOrbs;

    public Material lineMat;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(orbs.Count == 2)
        {
            if (orbs[0] != orbs[1])
            {
                OrbConnection newConnection = new OrbConnection(orbs[0], orbs[1]);

                string indexConnectionToRemove = connectionExists(orbs[0], orbs[1]);

                if (!string.IsNullOrEmpty(indexConnectionToRemove))
                {
                    int parsedIndex;
                    int.TryParse(indexConnectionToRemove, out parsedIndex);
                    connectedOrbs.RemoveAt(parsedIndex);
                    orbs[0].GetComponent<OrbController>().connectedOrbs.Remove(orbs[1]);
                    orbs[1].GetComponent<OrbController>().connectedOrbs.Remove(orbs[0]);
                }
                else
                {
                    connectedOrbs.Add(newConnection);
                    orbs[0].GetComponent<OrbController>().connectedOrbs.Add(orbs[1]);
                    orbs[1].GetComponent<OrbController>().connectedOrbs.Add(orbs[0]);
                }
            }
            orbs.Clear();
        }

        if(Input.GetMouseButton(2))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100) && !hit.transform.gameObject.CompareTag("Orb") ||
                !Physics.Raycast(ray, out hit, 100))
                orbs.Clear();
        }
	}

    public void addOrbTryConnection(GameObject orb)
    {
        orbs.Add(orb);
    }

    public string connectionExists(GameObject orb1, GameObject orb2)
    {
        int fakeIndex = 0;

        string indexConnectionToRemove = string.Empty;

        foreach (OrbConnection newConnection in connectedOrbs)
        {
            if ((newConnection.orb1 == orb1
                && newConnection.orb2 == orb2)
                ||
                (newConnection.orb1 == orb2
                && newConnection.orb2 == orb1))
            {
                indexConnectionToRemove = fakeIndex.ToString();
            }
            fakeIndex++;
        }

        return indexConnectionToRemove;
    }

    public void removeConnection(string index)
    {
        int parsedIndex;
        int.TryParse(index, out parsedIndex);
        connectedOrbs.RemoveAt(parsedIndex);
    }
}
