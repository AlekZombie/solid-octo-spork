﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbCameraController : CameraController {
    public GameObject currentOrb;

    public bool goingToOrb;

    public float speedH;
    public float speedV;
    private float yaw;
    private float pitch;

    public int initialFov;

    public int minFov;
    public int maxFov;

    // Use this for initialization
    void Start () {
        ownTag = "UI Orb";
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (GetComponent<Camera>().enabled)
        {
            if (Input.GetMouseButton(2))
            {
                yaw += speedH * Input.GetAxis("Mouse X");
                pitch -= speedV * Input.GetAxis("Mouse Y");
                if (pitch > 90f)
                    pitch = 90f;
                else if (pitch < -90)
                    pitch = -90f;
                transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            }

            if (goingToOrb)
            {
                goingToOrb = moveCameraTowards(currentOrb.transform.position);

                if (!goingToOrb)
                {
                    currentOrb.GetComponent<OrbController>().playVideo();
                    rotationAdjustment(0);
                }
            }

            //To reset camera position

            if (resettingPos)
            {
                resettingPos = moveCameraTowards(otherCamera.transform.position);
                if (!resettingPos)
                {
                    otherCamera.GetComponent<CameraController>().enableCamera();
                    currentOrb.GetComponent<OrbController>().panelName.SetActive(true);
                    currentOrb = null;
                }
            }

            //To zoom in
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                zoomIn();

            //To zoom out
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
                zoomOut();
        }
    }

    public new void activateResetPos()
    {
        currentOrb.GetComponent<OrbController>().stopVideo();
        Camera.main.fieldOfView = 50;
        currentOrb.GetComponent<OrbController>().switchSeeThrough("off");

        rotationAdjustment(-90);

        resettingPos = true;

        goingToOrb = false;

        transform.eulerAngles = otherCamera.transform.eulerAngles;

        otherCamera.GetComponent<CameraController>().enableCanvas();
    }

    public void setCurrentOrb(GameObject orb)
    {
        setFovToInitial();
        orb.GetComponent<OrbController>().setPrepared();
        orb.GetComponent<OrbController>().panelName.SetActive(false);
        if (!resettingPos)
        {
            if (currentOrb != null)
            {
                currentOrb.GetComponent<OrbController>().panelName.SetActive(true);
                currentOrb.GetComponent<OrbController>().pauseVideo();
                currentOrb.GetComponent<OrbController>().switchSeeThrough("off");
            }

            enableCamera();

            enableCanvas();

            this.currentOrb = orb;
            goingToOrb = true;
        }
    }

    public void currentOrbSeeThrough()
    {
        currentOrb.GetComponent<OrbController>().switchSeeThrough();
    }

    public void rotationAdjustment(int angle)
    {
        transform.eulerAngles = new Vector3(angle, transform.eulerAngles.y, transform.eulerAngles.z);

        foreach (GameObject orb in GameObject.FindGameObjectsWithTag("Orb"))
        {
            orb.transform.eulerAngles = new Vector3(angle, orb.transform.eulerAngles.y, orb.transform.eulerAngles.z);
        }
    }

    public new void zoomIn()
    {
        if (GetComponent<Camera>().fieldOfView > minFov && GetComponent<Camera>().fieldOfView <= maxFov)
        {
            GetComponent<Camera>().fieldOfView -= 1;
        }
    }

    public new void zoomOut()
    {
        if (GetComponent<Camera>().fieldOfView >= minFov && GetComponent<Camera>().fieldOfView < maxFov)
        {
            GetComponent<Camera>().fieldOfView += 1;
        }
    }

    void setFovToInitial()
    {
        GetComponent<Camera>().fieldOfView = initialFov;
    }


}
