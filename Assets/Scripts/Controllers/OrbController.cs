﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Video;

public class OrbController : MonoBehaviour {
    Vector3 screenPoint;
    Vector3 offset;

    bool isSeeThrough;

    public Texture orbTexture;

    Renderer rd;

    public string orbVideo;

    VideoPlayer vp;

    public List<GameObject> connectedOrbs;

    bool mouseDown, mouseUp;

    public int orbId;
    public string orbName;
    public string orbDescription;

    public GameObject panelName;

    private void Awake()
    {
        vp = GetComponent<VideoPlayer>();

    }

    // Use this for initialization
    void Start() {
        rd = GetComponent<Renderer>();
        rd.material.shader = Shader.Find("InsideShader");

        vp.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        vp.EnableAudioTrack(0, true);
        vp.SetTargetAudioSource(0, GetComponent<AudioSource>());

        isSeeThrough = false;

        mouseDown = false;
        mouseUp = false;
    }

    // Update is called once per frame
    void Update() {
        //Sets the texture stored in InsideTexture as the main texture
        if (orbTexture && GetComponent<Renderer>().material.mainTexture != orbTexture)
        {
            rd.material.mainTexture = orbTexture;
        }
    }

    public void createOrb(string name, string content, string description)
    {
        GameObject orb = Instantiate(this.gameObject) as GameObject;
        orbId = GameObject.FindGameObjectsWithTag("Orb").Length;
        orb.name = "Orb " + orbId;
        orb.GetComponent<OrbController>().orbName = name;
        orb.GetComponent<OrbController>().setVideo(content);
        orb.GetComponent<OrbController>().orbDescription = description;
        orb.GetComponent<OrbController>().panelName = Instantiate(panelName) as GameObject;
        orb.GetComponent<OrbController>().panelName.GetComponent<OrbName>().createPanelName(orb);
        }

    public void deleteOrb()
    {
        GameObject orbConnectionsController = GameObject.Find("Orb Connections");

        foreach (GameObject connectedOrb in connectedOrbs)
        {
            connectedOrb.GetComponent<OrbController>().connectedOrbs.Remove(this.gameObject);

            string indexToRemove = orbConnectionsController.GetComponent<OrbConnectionsController>().connectionExists(connectedOrb, this.gameObject);

            if (!string.IsNullOrEmpty(indexToRemove))
                orbConnectionsController.GetComponent<OrbConnectionsController>().removeConnection(indexToRemove);
        }

        Destroy(this.gameObject);

    }

    public void switchSeeThrough(string action = "toggle")
    {
        Color newColor = new Color();
        Color color = rd.material.color;

        switch(action)
        {
            case "toggle":
                if (!isSeeThrough)
                {
                    newColor = new Color(color.r, color.g, color.b, 0.3f);
                    //here move point (on) image
                    isSeeThrough = true;
                }
                else
                {
                    newColor = new Color(color.r, color.g, color.b, 1);
                    //here movie point (off) image
                    isSeeThrough = false;
                }
                break;
            case "on":
                newColor = new Color(color.r, color.g, color.b, 0.3f);
                //here move point (on) image
                isSeeThrough = true;
                break;
            case "off":
                newColor = new Color(color.r, color.g, color.b, 1);
                //here movie point (off) image
                isSeeThrough = false;
                break;
            default:
                if (!isSeeThrough)
                {
                    newColor = new Color(color.r, color.g, color.b, 0.3f);
                    //here move point (on) image
                    isSeeThrough = true;
                }
                else
                {
                    newColor = new Color(color.r, color.g, color.b, 1);
                    //here movie point (off) image
                    isSeeThrough = false;
                }
                break;
        }
        rd.material.color = newColor;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(2))
            mouseDown = true;

        if (mouseDown && Input.GetMouseButtonUp(2))
            mouseUp = true;

        if (Input.GetMouseButtonUp(1))
        {
            GameObject orbCamera = GameObject.Find("OrbCamera");
            GameObject currentOrb = orbCamera.GetComponent<OrbCameraController>().currentOrb;
            if (currentOrb == null
                || connectedOrbs.Contains(currentOrb))
                orbCamera.GetComponent<OrbCameraController>().setCurrentOrb(this.gameObject);
        }

        if (mouseUp)
        {
            GameObject.Find("Orb Connections").GetComponent<OrbConnectionsController>().addOrbTryConnection(this.gameObject);
            mouseDown = false;
            mouseUp = false;
        }

        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftControl))
            deleteOrb();
    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        if (GameObject.Find("Orb Connections").GetComponent<OrbConnectionsController>().orbs.Count != 1)
        {
            Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.WorldToScreenPoint(transform.position).z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint);
            transform.position = cursorPosition;
        }
    }

    void setVideo(string videoName)
    {
        vp.url = "file://" + Application.dataPath + "/Videos/" + videoName + ".mp4";
        //Set Audio Output to AudioSource
    }

    public void playVideo()
    {
        vp.Play();
        vp.enabled = true;
        GetComponent<AudioSource>().Play();
    }

    public void stopVideo()
    {
        vp.Stop();
        vp.enabled = false;
    }

    public void pauseVideo()
    {
        vp.Pause();
        vp.enabled = false;
    }

    public void setPrepared()
    {
        vp.Prepare();
    }

}
