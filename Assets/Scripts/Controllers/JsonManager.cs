﻿using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using UnityEngine.UI;
using System;

public class JsonManager : MonoBehaviour {

	private JsonData JsonToObj;
	public string JsonLocation;

	#region Json Methods
    public void SaveWorld(World world, string name)
    {
        JsonLocation = Application.dataPath + "/Json/"+name+".json";
        JsonData ObjToJson = JsonMapper.ToJson(world);
        File.WriteAllText(JsonLocation,ObjToJson.ToString());
        Debug.Log("Se ha guardado el mundo " + name);
    }

	public string GetNameWorld()
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/"+Global.GetComponent<VariablesGlobales>().JsonName+".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
		return (string)JsonToObj["WorldName"];
	}

	public string GetDescriptionWorld()
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (string)JsonToObj["WorldDesc"];
	}

    public int GetLenghtOfWorldPoints()
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (int)JsonToObj["WorldPoints"].Count;
    }

    public int GetLenghtOfConxObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return JsonToObj["WorldPoints"][i]["Conx"].Count;
    }

    public string GetNameObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (string)JsonToObj["WorldPoints"][i]["name"];
    }

    public float GetPosZObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (float)JsonToObj["WorldPoints"][i]["posz"];
    }

    public float GetPosXObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (float)JsonToObj["WorldPoints"][i]["posx"];
    }

    public string GetDescObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (string)JsonToObj["WorldPoints"][i]["description"];
    }

    public string GetImageObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (string)JsonToObj["WorldPoints"][i]["image"];
	}

	public string GetVideoObj(int i)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (string)JsonToObj["WorldPoints"][i]["video"];
	}

    public string GetConxObj(int i, int e)
    {
        GameObject Global = GameObject.Find("Global");
        JsonLocation = File.ReadAllText(Application.dataPath + "/Json/" + Global.GetComponent<VariablesGlobales>().JsonName + ".json");
        JsonToObj = JsonMapper.ToObject(JsonLocation);
        return (string)JsonToObj["WorldPoints"][i]["Conx"][e];
    }
    #endregion
}
