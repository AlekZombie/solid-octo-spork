﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {
    public GameObject addLocationForm;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	}

    public void switchAddOrbActive()
    {
        if (addLocationForm.activeSelf)
            addLocationForm.SetActive(false);
        else
            addLocationForm.SetActive(true);
    }
}
