﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.EventSystems;

public class OrbForms : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject orbPrefab;
    public InputField orbName;
    public Dropdown orbContent;
    public InputField orbDescription;

    public bool mouseOut = false;

    // Use this for initialization
    private void Awake()
    {
        SetCmbVideo();
    }

    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (mouseOut && (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)))
            this.gameObject.SetActive(false);
	}

    void SetCmbVideo()
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.dataPath + "/Videos");
        FileInfo[] fileInfo = directoryInfo.GetFiles("*.mp4", SearchOption.AllDirectories);
        orbContent.GetComponent<Dropdown>().options.Clear();
        foreach (FileInfo file in fileInfo)
        {
            string nameFile = Path.GetFileNameWithoutExtension(file.FullName);
            Dropdown.OptionData optionData = new Dropdown.OptionData(nameFile);
            orbContent.GetComponent<Dropdown>().options.Add(optionData);
        }
        orbContent.RefreshShownValue();
    }

    public void submitOrbForm()
    {
        orbPrefab.GetComponent<OrbController>().createOrb(orbName.transform.Find("Text").gameObject.GetComponent<Text>().text,
            orbContent.transform.Find("Label").gameObject.GetComponent<Text>().text,
            orbDescription.transform.Find("Text").gameObject.GetComponent<Text>().text);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOut = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOut = true;
    }
}
