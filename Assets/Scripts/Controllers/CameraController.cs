﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public bool resettingPos;

    public GameObject otherCamera;

    public GameObject ownCanvas;

    public string ownTag;

    public void activateResetPos()
    {
        resettingPos = true;
    }

    public bool moveCameraTowards(Vector3 position)
    {
        float cameraPositionsDistance = (((transform.position - position).magnitude / 10) >= 0.01f ?
            ((transform.position - position).magnitude / 10) : 0.01f);

        transform.position = Vector3.MoveTowards(transform.position, position, cameraPositionsDistance);

        return transform.position != position;
    }


    public void zoomIn()
    {
        if (transform.position.y - 1 >= 2)
            transform.position -= new Vector3(0, 1, 0);
    }

    public void zoomOut()
    {
        if (transform.position.y + 1 <= 20)
            transform.position += new Vector3(0, 1, 0);
    }

    public void enableCanvas()
    {
        ownCanvas.GetComponent<Canvas>().enabled = true;
        otherCamera.GetComponent<CameraController>().ownCanvas.GetComponent<Canvas>().enabled = false;
    }

    public void enableCamera()
    {
        GetComponent<Camera>().enabled = true;
        otherCamera.GetComponent<Camera>().enabled = false;
        foreach (GameObject ui in GameObject.FindGameObjectsWithTag(otherCamera.GetComponent<CameraController>().ownTag))
        {
            ui.SetActive(false);
        }
    }
}
