﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InterfacePoint : MonoBehaviour
{
    /* attributes for unity 
    private GameObject connectingPoint;
    GameObject point;
    private Vector3 screenPoint;
    private Vector3 offset;
    private GameObject connectedPoint;
    public bool beingClicked;
    private Texture defaultParticle;
    public Texture InsideTexture;
    private bool areConnected;
    public bool transparent;
    private GameObject principalMenu;
    private bool isShow;
    public int id;
    public string insideTextureName;

    void Start()
    {
        principalMenu = GameObject.Find("Principal");
        transparent = false;
        beingClicked = false;
        areConnected = false;
        isShow = false;
    }

    void OnMouseDown()
    {
        if (SceneManager.GetActiveScene().name.Equals("Editor"))
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }
    }

    //and now the point is being dragged
    void OnMouseDrag()
    {
        if (SceneManager.GetActiveScene().name.Equals("Editor"))
        {
            Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            transform.position = cursorPosition;
        }
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(1))
        {
            beingClicked = true;
            GameObject worldController = GameObject.Find("World Controller");
            if (worldController.GetComponent<WorldController>().currentSphere != null)
            {
                areConnected = Camera.main.GetComponent<MapPoint>().areConnected(this.gameObject);
                if (areConnected)
                {
                    //worldController.GetComponent<WorldController>().currentSphere.GetComponent<InterfacePoint>().pauseMovie();
                }
            }
        }
        if (Input.GetMouseButtonUp(2) && !Camera.main.GetComponent<CamController>().isInsideMode())
        {
            Camera.main.GetComponent<MapPoint>().newConnect();
        }
        if (Input.GetMouseButtonUp(1))
        {
            GameObject radialMenu = GameObject.Find("RadialMenu Canvas");
            if (!isShow)
            {
                Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
                principalMenu.transform.position = mousePosition;
                radialMenu.GetComponent<EditMenu>().pointToEdit = gameObject;
                isShow = true;
            }
            else
            {
                principalMenu.transform.position = new Vector3(-3076, 0, 0);
                radialMenu.GetComponent<EditMenu>().pointToEdit = null;
                isShow = false;
            }
        }
    }

    public void makeVisible()
    {
        if (SceneManager.GetActiveScene().name.Equals("Editor"))
        {
            GameObject worldController = GameObject.Find("World Controller");
            transparent = false;
            var color = worldController.GetComponent<WorldController>().currentSphere.GetComponent<Renderer>().material.color;
            var newColor = new Color(color.r, color.g, color.b, 1f);
            worldController.GetComponent<WorldController>().currentSphere.GetComponent<Renderer>().material.color = newColor;
        }
    }
    */
}
