﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;

public class CamController : MonoBehaviour
{
    /*
    public float speedH;
    public float speedV;
    private float yaw;
    private float pitch;
    private bool resetCamPosActive;
    private Vector3 initialCamPos;
    private Vector3 initialCamEuler;
    private GameObject[] points;
    private bool insideMode;
    private Vector3 camOldPos;
    private Vector3 mouseOldPos;
    private Ray ray2;
    private RaycastHit hit2;
    private bool hit3;
    private bool isItMoving;

    // Use this for initialization
    void Start(){ 
        insideMode = false;
        initialCamEuler = transform.eulerAngles;
        initialCamPos = transform.position;
        speedH = 5f;
        speedV = 5f;
        yaw = 0.0f;
        pitch = 0.0f;
        resetCamPosActive = false;
    }


    // Update is called once per frame
    void Update(){
        //camera movement
        if (Input.GetMouseButton(2))
        {
            if (insideMode)
            {
                yaw += speedH * Input.GetAxis("Mouse X");
                pitch -= speedV * Input.GetAxis("Mouse Y");
                if (pitch > 90f)
                {
                    pitch = 90f;
                }
                else if (pitch < -90)
                {
                    pitch = -90f;
                }
                transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            }
            else if (!isItMoving)
            {
                if (Input.GetMouseButtonDown(2))
                {
                    camOldPos = transform.position;
                    mouseOldPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }
                Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition) - mouseOldPos;
                pos = new Vector3(pos.x, 0, pos.y);
                transform.position = camOldPos + -pos * 20f;
            }
        }

        if (resetCamPosActive)
        {
            resetCamPosMove();
        }

    }


    // resets camera position
    public void resetCamPoS(){
        GameObject worldController = GameObject.Find("World Controller");
        if (Camera.main.transform.position != initialCamPos)
        {
            if (worldController.GetComponent<WorldController>().currentSphere != null)
            {
				worldController.GetComponent<WorldController>().currentSphere.GetComponent<InterfacePoint>().makeVisible();
                
            }
            Debug.Log("not efficient 2");
            isItMoving = true;
            yaw = 0.0f;
            pitch = 0.0f;
            transform.eulerAngles = initialCamEuler;
            foreach (GameObject point in GameObject.FindGameObjectsWithTag("Location Point"))
            {
                Debug.Log("not efficient 223232323");
                point.transform.eulerAngles = new Vector3(90, point.transform.eulerAngles.y, point.transform.eulerAngles.z);
            }
            this.insideMode = false;
            if (this.transform.position != initialCamPos)
            {
                resetCamPosActive = true;
                worldController.GetComponent<WorldController>().currentSphere = null;
            }
        }
    }

    public void resetCamPosMove(){
        Debug.Log("not efficient 3233");
        transform.position = Vector3.MoveTowards(transform.position, initialCamPos, 0.5f);
        if (this.transform.position == initialCamPos)
        {
            resetCamPosActive = false;
            isItMoving = false;
        }
    }

    public void stopAllVideos(){
        foreach (GameObject gobject in GameObject.FindGameObjectsWithTag("Location Point"))
        {
			//gobject.GetComponent<InterfacePoint>().pauseMovie();
        }
    }
    //takes the camera to the position of the clicked point
    public void goToSphere(Vector3 position, float speed, GameObject gobject){
        GameObject worldController = GameObject.Find("World Controller");
        if (worldController.GetComponent<WorldController>().currentSphere != null)
        {
			worldController.GetComponent<WorldController>().currentSphere.GetComponent<InterfacePoint>().makeVisible();
        }
        isItMoving = true;
        transform.position = Vector3.MoveTowards(this.transform.position, position, speed);
        if (transform.position == position)
        {
            Debug.Log("not efficient DUUUDE");
            insideMode = true;
            foreach (GameObject point in GameObject.FindGameObjectsWithTag("Location Point"))
            {
                point.transform.eulerAngles = new Vector3(0, point.transform.eulerAngles.y, point.transform.eulerAngles.z);
            }
            isItMoving = false;
			gobject.GetComponent<InterfacePoint>().beingClicked = false;
            worldController.GetComponent<WorldController>().currentSphere = gobject;
        }
    }

    public void setInsideMode(bool insideMode)
    {
        this.insideMode = insideMode;
    }

    public bool isInsideMode()
    {
        return insideMode;
    }
    public void setCamAngle(){
        if (insideMode){
            transform.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, Camera.main.transform.eulerAngles.z);
        }
    }

    public void ChangeScene(string level)
    {
        Application.LoadLevel(level);
    }

    public void makeTransparentToggle()
    {
        GameObject worldController = GameObject.Find("World Controller");
        var newColor = new Color();
        var color = worldController.GetComponent<WorldController>().currentSphere.GetComponent<Renderer>().material.color;
		if (!worldController.GetComponent<WorldController>().currentSphere.GetComponent<InterfacePoint>().transparent)
        {
            newColor = new Color(color.r, color.g, color.b, 0.3f);
            //here move point (on) image
			worldController.GetComponent<WorldController>().currentSphere.GetComponent<InterfacePoint>().transparent = true;
        }
        else {
            newColor = new Color(color.r, color.g, color.b, 1);
            //here movie point (off) image
			worldController.GetComponent<WorldController>().currentSphere.GetComponent<InterfacePoint>().transparent = false;
        }
        worldController.GetComponent<WorldController>().currentSphere.GetComponent<Renderer>().material.color = newColor;
    }

    public void zoomIn()
    {
        if(GetComponent<Camera>().fieldOfView > 5 && GetComponent<Camera>().fieldOfView <= 175)
        {
            GetComponent<Camera>().fieldOfView -= 5; 
        }   
    }

    public void zoomOut()
    {
        if (GetComponent<Camera>().fieldOfView >= 5 && GetComponent<Camera>().fieldOfView < 175)
        {
            GetComponent<Camera>().fieldOfView += 5;
        }
    }
    */
}