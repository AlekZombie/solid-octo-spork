﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrbName : MonoBehaviour {
    public GameObject orb;
    public Text orbname;
	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void LateUpdate() {
        if (orb) {
            if (Camera.main.WorldToViewportPoint(orb.transform.position).x > 0 &&
               Camera.main.WorldToViewportPoint(orb.transform.position).y > 0 &&
               Camera.main.WorldToViewportPoint(orb.transform.position).x <= 1 &&
               Camera.main.WorldToViewportPoint(orb.transform.position).y <= 1 &&
               Camera.main.WorldToViewportPoint(orb.transform.position).z > 0)
            {
                if (Camera.main.name.Equals("WorldCamera"))
                {
                    if(GetComponent<CanvasGroup>().alpha == 0)
                        GetComponent<CanvasGroup>().alpha = 1;
                    transform.position = new Vector3(Camera.main.WorldToScreenPoint(orb.transform.GetChild(0).position).x,
                        Camera.main.WorldToScreenPoint(orb.transform.GetChild(0).position).y - 20);
                }
                else
                {
                    if (Camera.main.GetComponent<OrbCameraController>().currentOrb.GetComponent<OrbController>().connectedOrbs.Contains(orb))
                    {
                        if (GetComponent<CanvasGroup>().alpha == 0)
                            GetComponent<CanvasGroup>().alpha = 1;
                        transform.position = new Vector3(Camera.main.WorldToScreenPoint(orb.transform.position).x,
                            Camera.main.WorldToScreenPoint(orb.transform.position).y);
                    }
                }
            } else
                GetComponent<CanvasGroup>().alpha = 0;
        }
        else
            Destroy(this.gameObject);
    }

    public void createPanelName(GameObject refOrb) {
        orb = refOrb;
        orbname.text = refOrb.GetComponent<OrbController>().orbName;
        transform.parent = GameObject.Find("OrbNames").transform;
    }
}
