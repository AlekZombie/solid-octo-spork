﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class WorldController : MonoBehaviour {
    /*

	public World world {get; set;}
	bool isCreated=false;
    public static int ID = 0;
    public GameObject currentSphere;
    public Button saveWorld;


    // Use this for initialization
    public void OnEnable () {
        world = new World();
	}

    public void Start()
    {
        if (SceneManager.GetActiveScene().name.Equals("Editor"))
        {
            saveWorld.onClick.AddListener(delegate {
                SaveWorld(world);
            });
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (!SceneManager.GetActiveScene().name.Equals("Editor")) {
            if (!isCreated)
            {
                world.CreateSphere();
                isCreated = true;
            }
        }
	}

    public int GiveID()
    {
        ID++;
        return ID;
    }
    /*
    public void addPoint()
    {
        GameObject point = Instantiate(Resources.Load("Prefabs/Sphere")) as GameObject;
        point.name = "Esfera " + GameObject.FindGameObjectsWithTag("Location Point").Length;
		point.transform.eulerAngles = new Vector3 (90, transform.eulerAngles.y, transform.eulerAngles.z);
        point.GetComponent<InterfacePoint>().id = GiveID();
    }

    public void addPointToWorld()
    {   
        GameObject principal = GameObject.Find("Principal");
        GameObject editMenu = GameObject.Find("RadialMenu Canvas");
        GameObject point = editMenu.GetComponent<EditMenu>().pointToEdit;
        if (isExist(point.GetComponent<InterfacePoint>().id))
        {
            int fakeIndex;
            fakeIndex = world.Modify(point);
            world.WorldPoints[fakeIndex].posx = point.transform.position.x;
            world.WorldPoints[fakeIndex].posz = point.transform.position.z;
			world.WorldPoints[fakeIndex].video = string.IsNullOrEmpty(point.GetComponent<UnityEngine.Video.VideoPlayer>().url) ? "" : point.GetComponent<UnityEngine.Video.VideoPlayer>().url;
			world.WorldPoints[fakeIndex].image = point.GetComponent<InterfacePoint>().InsideTexture ? point.GetComponent<InterfacePoint>().insideTextureName : "";
			world.WorldPoints[fakeIndex].description = point.GetComponent<ToolTip>().toolTipText;
			world.WorldPoints[fakeIndex].icon = point.GetComponent<ToolTip>().icon.name;
            Debug.Log("Se ha modificado la esfera");
        }
        else
        {
            Point p = new Point();
            p.name = point.name;
            p.id = point.GetComponent<InterfacePoint>().id;
            p.posx = point.transform.position.x;
            p.posz = point.transform.position.z;
            p.video = string.IsNullOrEmpty(point.GetComponent<UnityEngine.Video.VideoPlayer>().url) ? "" : point.GetComponent<UnityEngine.Video.VideoPlayer>().url;
            p.image = point.GetComponent<InterfacePoint>().InsideTexture ? point.GetComponent<InterfacePoint>().insideTextureName : "";
			p.description = point.GetComponent<ToolTip>().toolTipText;
			p.icon = point.GetComponent<ToolTip>().icon.name;
            Debug.Log("Se ha creado la esfera");
            world.addSphere(p);
        }
        principal.transform.position = new Vector3(-3076, 0, 0);
    }

    public void makeVisiblePoint()
    {
        if (SceneManager.GetActiveScene().name.Equals("Editor"))
        {
            var color = currentSphere.GetComponent<Renderer>().material.color;
            var newColor = new Color(color.r, color.g, color.b, 1f);
            currentSphere.GetComponent<Renderer>().material.color = newColor;
        }
    }

    public void SetWorldName(string guess)
    {
        world.WorldName = guess;
    }

    public bool isExist(int ID)
    {
        bool aux = false;
        foreach (Point p in world.WorldPoints)
        {
            if (p.id == ID)
            {
                aux = true;
            }
        }
        return aux;
    }

    public void makeTransparentToggle()
    {
        GameObject worldController = GameObject.Find("World Controller");
        var newColor = new Color();
        var color = currentSphere.GetComponent<Renderer>().material.color;
        if (!currentSphere.GetComponent<InterfacePoint>().transparent)
        {
            newColor = new Color(color.r, color.g, color.b, 0.3f);
            //here move point (on) image
            currentSphere.GetComponent<InterfacePoint>().transparent = true;
        }
        else {
            newColor = new Color(color.r, color.g, color.b, 1);
            //here movie point (off) image
            currentSphere.GetComponent<InterfacePoint>().transparent = false;
        }
        currentSphere.GetComponent<Renderer>().material.color = newColor;
    }

    public void SaveWorld(World world)
    {
        GameObject jsonManager = GameObject.Find("Json Controller");
        jsonManager.GetComponent<JsonManager>().SaveWorld(world, world.WorldName);
    }
    */
}
