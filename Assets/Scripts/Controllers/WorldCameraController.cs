﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCameraController : CameraController{

    Vector3 oldMousePos;
    Vector3 oldCameraPos;

    Vector3 initialCameraPos;

	// Use this for initialization
	void Start () {
        initialCameraPos = transform.position;
        resettingPos = false;
        ownTag = "UI World";
	}
	
	// Update is called once per frame
	void Update () {

    }

    void LateUpdate()
    {
        if (GetComponent<Camera>().enabled)
        {
            if(otherCamera.transform.position != transform.position)
                otherCamera.transform.position = transform.position;
            //Mouse Camera Movement
            if (Input.GetMouseButton(2))
            {
                resettingPos = false;//If want to move camera, cancel resetPos

                if (Input.GetMouseButtonDown(2))
                {
                    oldCameraPos = transform.position;
                    oldMousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }

                Vector3 newCameraPos = Camera.main.ScreenToViewportPoint(Input.mousePosition) - oldMousePos;
                newCameraPos = new Vector3(newCameraPos.x, 0, newCameraPos.y);
                transform.position = oldCameraPos + -newCameraPos * 20f;
            }

            //To reset camera position
            if (resettingPos)
                resettingPos = moveCameraTowards(initialCameraPos);

            if (!resettingPos)
            {
                //To zoom in
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                    zoomIn();

                //To zoom out
                if (Input.GetAxis("Mouse ScrollWheel") < 0)
                    zoomOut();
            }
        }
    }
}
