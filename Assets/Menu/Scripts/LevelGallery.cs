﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class LevelGallery : MonoBehaviour {

	public GameObject buttonPrefab;
	public GameObject Parent;
	GameObject Global;

	private void Start(){
		Global = GameObject.Find ("Global");
		string[] jsons = Directory.GetFiles(Application.dataPath+"/Json","*.json");

        foreach (string namejson in jsons) {
			string nameFile = Path.GetFileNameWithoutExtension(namejson);
			GameObject go = Instantiate (buttonPrefab) as GameObject;
			go.transform.SetParent (Parent.transform);
            go.GetComponentInChildren<Text>().text = nameFile;
            go.GetComponent<Button>().onClick.AddListener(() => OnButtonClick(nameFile));
            
		}
	}

    public void OnButtonClick(string namejson){
        Application.LoadLevel("LoadJson");
		Global.GetComponent<VariablesGlobales> ().JsonName = namejson;

    }
}
