﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
	public Transform mainMenu, levelMenu;

	public void ChangeScene(string sceneName){
		Application.LoadLevel (sceneName);
	}

	public void QuitGame(){
		Application.Quit();
	}

	public void LevelMenu(bool clicked){
		if(clicked){
			levelMenu.gameObject.SetActive (clicked);
			mainMenu.gameObject.SetActive (false);
		}else{
			levelMenu.gameObject.SetActive(clicked);
			mainMenu.gameObject.SetActive(true);
		}
	}
}